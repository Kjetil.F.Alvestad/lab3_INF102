package INF102.lab3.sumList;

import java.util.List;

public class SumRecursive implements ISum {

    @Override
    public long sum(List<Long> list) {
        long sum = 0;
        if (!list.isEmpty()) {
            sum += list.remove(0);
            if (!list.isEmpty()) {
                sum += sum(list);
            }
        }
        return sum;
    }
}