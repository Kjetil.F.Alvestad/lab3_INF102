package INF102.lab3.numberGuesser;

public class MyGeniusGuesser implements IGuesser {

  @Override
  public int findNumber(RandomNumber number) {
    int lowerbound = number.getLowerbound();
    int upperbound = number.getUpperbound();

    int guess = ((upperbound - lowerbound) / 2) + lowerbound;

    int guessCheck = number.guess(guess);

    while (guessCheck != 0) {
      if (guessCheck == -1) {
        lowerbound = guess;
        guess = ((upperbound - lowerbound) / 2) + lowerbound;
      } else if (guessCheck == 1) {
        upperbound = guess;
        guess = ((upperbound - lowerbound) / 2) + lowerbound;
      }

      guessCheck = number.guess(guess);
    }

    return guess;
  }
}
