package INF102.lab3.peakElement;

import java.util.List;

public class PeakRecursive implements IPeak {

  @Override
  public int peakElement(List<Integer> numbers) {
    if (numbers.isEmpty()) throw new IllegalArgumentException("Empty list");

    // Edge cases
    int n = numbers.size();
    if (n == 1) return numbers.get(0);

    if (numbers.get(0) > numbers.get(1)) return numbers.get(0);

    if (numbers.get(n - 1) > numbers.get(n - 2)) return numbers.get(n - 1);

    if (n > 2) {
      int current = numbers.get(0);
      int next = numbers.get(1);
      int nextnext = numbers.get(2);

      if (isPeak(current, next, nextnext)) {
        return next;
      } else if (n > 3) {
        numbers.remove(0);
        return peakElement(numbers);
      }
    }
    throw new IllegalArgumentException("No peak element");
  }

  private boolean isPeak(int prev, int current, int next) {
    return current > prev && current > next;
  }
}
